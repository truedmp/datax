import unittest
from unittest import TestCase
from unittest.mock import patch

from datax.helpers.google_storage_client import get_storage_client


class TestHelpersGoogleStorageClient(TestCase):
    @patch('datax.helpers.google_storage_client.open')
    @patch('datax.helpers.google_storage_client.json.load')
    @patch('datax.helpers.google_storage_client.storage')
    @patch('datax.helpers.google_storage_client.ServiceAccountCredentials')
    def test_get_storage_client_call_service_account(self, mock_service_account, mock_storage, mock_json_load, mock_open_file):
        credentials = {
            "type": "service_account",
            "project_id": "my_project",
            "private_key_id": "test_private_key_id",
            "private_key": "test_private_key",
            "client_email": "test_client_email@test.com",
            "client_id": "test_client_id",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "test_client_cert_url"
        }
        mock_json_load.return_value = credentials

        expected_credentials_dict = {
            'type': 'service_account',
            'client_id': credentials['client_id'],
            'client_email': credentials['client_email'],
            'private_key': credentials['private_key'],
            'private_key_id': credentials['private_key_id']
        }
        get_storage_client("my_project", "/path/to/credentials")

        mock_service_account.from_json_keyfile_dict.assert_called_once_with(expected_credentials_dict)
    
    @patch('datax.helpers.google_storage_client.open')
    @patch('datax.helpers.google_storage_client.json.load')
    @patch('datax.helpers.google_storage_client.storage')
    @patch('datax.helpers.google_storage_client.ServiceAccountCredentials')
    def test_get_storage_client_call_storage_client(self, mock_service_account, mock_storage, mock_json_load, mock_open_file):
        credentials = "credentials"
        project = "my_project"

        mock_service_account.from_json_keyfile_dict.return_value = credentials

        get_storage_client(project, credentials)

        mock_storage.Client.assert_called_once_with(
            credentials=credentials,
            project=project
        )


if __name__ == "__main__":
    unittest.main()