import pathlib
import unittest
from unittest import TestCase
from unittest.mock import MagicMock, patch, call

import pandas as pd

from datax import GCS


class TestGCS(TestCase):
    def test_private_upload_file(self):
        mock_bucket = MagicMock()
        mock_blob = mock_bucket.blob.return_value
        expect_file = "my_file"
        expect_dest = "destination"
        GCS()._upload_file(mock_bucket, expect_file, expect_dest)

        mock_bucket.blob.assert_called_once_with(expect_dest)
        mock_blob.upload_from_filename.assert_called_once_with(expect_file, content_type=None)

    @patch("datax.services.gcs.StringIO")
    def test_private_upload_dataframe(self, mock_content):
        mock_bucket = MagicMock()
        mock_blob = mock_bucket.blob.return_value
        stub_conent = mock_content.return_value
        stub_content_value = stub_conent.getvalue.return_value
        mock_file = MagicMock()
        expect_dest = "destination"

        GCS()._upload_dataframe(mock_bucket, mock_file, expect_dest)

        mock_file.to_csv.assert_called_once_with(stub_conent, index=False, header=False)        
        mock_bucket.blob.assert_called_once_with(expect_dest)
        mock_blob.upload_from_string.assert_called_once_with(stub_content_value, content_type="text/csv")

    @patch("datax.services.gcs.StringIO")
    def test_private_upload_dataframe_with_header(self, mock_content):
        mock_bucket = MagicMock()
        mock_blob = mock_bucket.blob.return_value
        stub_conent = mock_content.return_value
        stub_content_value = stub_conent.getvalue.return_value
        mock_file = MagicMock()
        expect_dest = "destination"

        GCS()._upload_dataframe(mock_bucket, mock_file, expect_dest, header=True)

        mock_file.to_csv.assert_called_once_with(stub_conent, index=False, header=True)
    
    @patch("datax.services.gcs.StringIO")
    def test_private_upload_dataframe_with_index(self, mock_content):
        mock_bucket = MagicMock()
        mock_blob = mock_bucket.blob.return_value
        stub_conent = mock_content.return_value
        stub_content_value = stub_conent.getvalue.return_value
        mock_file = MagicMock()
        expect_dest = "destination"

        GCS()._upload_dataframe(mock_bucket, mock_file, expect_dest, index=True)

        mock_file.to_csv.assert_called_once_with(stub_conent, index=True, header=False)

    @patch("datax.services.gcs.GCS._upload_file")
    def test_upload_with_path_should_call_upload_file(self, mock_upload_file):
        mock_bucket = MagicMock()
        expected_path = "path/to/file"
        expected_dest = "path/to/destination"

        actual = GCS().upload(mock_bucket, expected_path, expected_dest)

        mock_upload_file.assert_called_once_with(mock_bucket, expected_path, expected_dest, None)
        self.assertTrue(actual)

    @patch("datax.services.gcs.GCS._upload_dataframe")
    def test_upload_with_dataframe_should_call_upload_dataframe(self, mock_upload_dataframe):
        mock_bucket = MagicMock()
        expected_df = pd.DataFrame([{"test": "test_value"}])
        expected_dest = "path/to/destination"

        actual = GCS().upload(mock_bucket, expected_df, expected_dest)

        mock_upload_dataframe.assert_called_once_with(mock_bucket, expected_df, expected_dest, False, False)
        self.assertTrue(actual)

    @patch("datax.services.gcs.str")
    @patch("datax.services.gcs.StringIO")
    def test_private_download_file_as_string(self, mock_string_io, mock_str):
        mock_blob = MagicMock()
        mock_blob.download_as_string.return_value = b"string"
        expected = "string"
        mock_str.return_value = expected
        actual = GCS()._download_file_as_string(mock_blob)

        mock_blob.download_as_string.assert_called_once()
        mock_str.assert_called_once_with(b"string", "utf-8")
        mock_string_io.assert_called_once_with(expected)

        self.assertEqual(actual, mock_string_io.return_value)

    @patch("datax.services.gcs.str")
    @patch("datax.services.gcs.StringIO")
    def test_private_download_file_as_string_when_utf_8_error(self, mock_string_io, mock_str):
        mock_blob = MagicMock()
        mock_blob.download_as_string.return_value = b"string"
        mock_str.side_effect = [TypeError, "string"]
        expected = "string"
        mock_str.return_value = expected
        expected_calls = [call(b"string", "utf-8"), call(b"string")]
        actual = GCS()._download_file_as_string(mock_blob)

        actual_calls = mock_str.call_args_list
        mock_string_io.assert_called_once_with(expected)

        self.assertEqual(actual_calls, expected_calls)
        self.assertEqual(actual, mock_string_io.return_value)

    def test_get_blobs(self):
        bucket = MagicMock()
        path = "path/to/data"
        actual = GCS()._get_blobs(bucket, path)
        expected = list(bucket.list_blobs.return_value)
        self.assertEqual(expected, actual)

    def test_get_blobs_should_list_blobs_from_path(self):
        bucket = MagicMock()
        path = "path/to/data"
        GCS()._get_blobs(bucket, path)
        bucket.list_blobs.assert_called_once_with(prefix=path)

    def test_get_file_extension(self):
        expected = ".csv"
        blob_1 = MagicMock()
        blob_1.name = "path/to/blob_1.csv"
        blob_2 = MagicMock()
        blob_2.name = "path/to/blob_2.csv"
        blobs = [blob_1, blob_2]
        actual = GCS()._get_file_extension(blobs)
        self.assertEqual(expected, actual)

    def test_get_file_extension_with_different_type(self):
        blob_1 = MagicMock()
        blob_1.name = "path/to/blob_1.csv"
        blob_2 = MagicMock()
        blob_2.name = "path/to/blob_2.json"
        blobs = [blob_1, blob_2]
        with self.assertRaises(TypeError):
            GCS()._get_file_extension(blobs)

    @patch("datax.services.gcs.pathlib")
    def test_download_to_destination(self, mock_pathlib):
        filename = MagicMock()
        filename.name = "filename"
        output_path = MagicMock(specs=pathlib)
        mock_pathlib.Path.side_effect = filename, output_path
        bucket = MagicMock()
        blob = MagicMock()
        GCS()._download_to_destination(bucket, blob, destination="output_path")
        bucket.blob.assert_called_once_with(blob.name)
        output_path.joinpath.assert_called_once_with(filename.name)
        bucket.blob.return_value.download_to_filename.assert_called_once_with(output_path.joinpath.return_value)

    @patch("datax.services.gcs.pathlib")
    def test_download_to_destination_should_mkdir(self, mock_pathlib):
        filename = MagicMock()
        filename.name = "filename"
        output_path = MagicMock(specs=pathlib)
        mock_pathlib.Path.side_effect = filename, output_path
        bucket = MagicMock()
        blob = MagicMock()
        GCS()._download_to_destination(bucket, blob, destination="output_path")
        output_path.mkdir.assert_called_once_with(parents=True, exist_ok=True)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_get_blobs(self, mock_datax, mock_download_to_destination,
                                            mock_download_file_as_string, mock_get_file_extension, mock_get_blobs):
        bucket = MagicMock()
        path = "path/to/file"
        GCS().download(bucket, path)
        mock_get_blobs.assert_called_once_with(bucket, path)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_get_file_extensions(self, mock_datax, mock_download_to_destination,
                                                      mock_download_file_as_string, mock_get_file_extension,
                                                      mock_get_blobs):
        bucket = MagicMock()
        path = "path/to/file"
        GCS().download(bucket, path)
        mock_get_file_extension.assert_called_once_with(mock_get_blobs.return_value)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_download_file_as_string(self, mock_datax, mock_download_to_destination,
                                                          mock_download_file_as_string, mock_get_file_extension,
                                                          mock_get_blobs):
        bucket = MagicMock()
        blob_1 = MagicMock()
        blob_1.name = "blob_1"
        blob_2 = MagicMock()
        blob_2.name = "blob_2"
        mock_get_blobs.return_value = [blob_1, blob_2]
        path = "path/to/file"
        GCS().download(bucket, path)
        actual = mock_download_file_as_string.call_args_list
        expected = [call(bucket.blob.return_value), call(bucket.blob.return_value)]
        self.assertEqual(expected, actual)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_bucket_blob_2_times(self, mock_datax, mock_download_to_destination,
                                                      mock_download_file_as_string, mock_get_file_extension,
                                                      mock_get_blobs):
        bucket = MagicMock()
        blob_1 = MagicMock()
        blob_1.name = "blob_1"
        blob_2 = MagicMock()
        blob_2.name = "blob_2"
        mock_get_blobs.return_value = [blob_1, blob_2]
        path = "path/to/file"
        GCS().download(bucket, path)
        actual = bucket.blob.call_count
        expected = 2
        self.assertEqual(expected, actual)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_download_to_destination(self, mock_datax, mock_download_to_destination,
                                                          mock_download_file_as_string, mock_get_file_extension,
                                                          mock_get_blobs):
        bucket = MagicMock()
        blob_1 = MagicMock()
        blob_1.name = "blob_1"
        blob_2 = MagicMock()
        blob_2.name = "blob_2"
        mock_get_blobs.return_value = [blob_1, blob_2]
        path = "path/to/file"
        output_path = "output_path"
        GCS().download(bucket, path, output_path=output_path)
        actual = mock_download_to_destination.call_args_list
        expected = [call(bucket, blob_1, output_path), call(bucket, blob_2, output_path)]
        self.assertEqual(expected, actual)

    @patch("datax.services.gcs.GCS._get_blobs")
    @patch("datax.services.gcs.GCS._get_file_extension")
    @patch("datax.services.gcs.GCS._download_file_as_string")
    @patch("datax.services.gcs.GCS._download_to_destination")
    @patch("datax.services.gcs.Datax")
    def test_download_should_call_datax(self, mock_datax, mock_download_to_destination,
                                        mock_download_file_as_string, mock_get_file_extension,
                                        mock_get_blobs):
        bucket = MagicMock()
        blob_1 = MagicMock()
        blob_1.name = "blob_1"
        blob_2 = MagicMock()
        blob_2.name = "blob_2"
        mock_get_blobs.return_value = [blob_1, blob_2]
        mock_get_file_extension.return_value = ".csv"
        mock_download_file_as_string.side_effect = ["content1", "content2"]
        path = "path/to/file"
        GCS().download(bucket, path)
        mock_datax.assert_called_once_with(['content1', 'content2'], data_type=".csv")

if __name__ == "__main__":
    unittest.main()
