import unittest
from unittest import TestCase
from unittest.mock import MagicMock, patch

from datax import CMS
from datax.model import Datax


class TestCMS(TestCase):
    def test_dsl_should_has_correct_format(self):
        es_session = MagicMock()
        es_index = MagicMock()
        expected = {
            "query": {
                "bool": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"match": {"movie_type": "movie"}},
                                {"match": {"ep_master": "Y"}}
                            ]
                        }
                    }
                }
            }
        }
        cms = CMS()
        cms.download(es_session, es_index, movie_type="movie", ep_master="Y")
        self.assertEqual(cms.dsl, expected)

    def test_download_should_return_datax_object(self):
        es_session = MagicMock()
        es_index = MagicMock()
        cms = CMS()
        actual = cms.download(es_session, es_index)
        self.assertTrue(isinstance(actual, Datax))

    def test_add_must_conditions(self):
        cms = CMS()
        expected = {
            "query": {
                "bool": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"match": {"movie_type": "movie"}},
                                {"match": {"ep_master": "Y"}}
                            ]
                        }
                    }
                }
            }
        }
        dsl = {"query": {"bool": {"filter": {"bool": {"must": []}}}}}

        actual = cms._add_must_conditions(dsl, movie_type="movie", ep_master="Y")
        self.assertEqual(actual, expected)

    def test_add_must_conditions_with_no_condition(self):
        cms = CMS()
        expected = {
            "query": {
                "bool": {
                    "filter": {
                        "bool": {
                            "must": []
                        }
                    }
                }
            }
        }
        dsl = {"query": {"bool": {"filter": {"bool": {"must": []}}}}}

        actual = cms._add_must_conditions(dsl)
        self.assertEqual(actual, expected)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        default_size = 10000
        cms = CMS()
        cms.download(es_session, es_index)

        _, actual = mock_scan.call_args
        expected = {'query': mock_dsl, 'index': 'test_index', '_source': None, 'size': 100}
        self.assertEqual(actual, expected)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan_with_required_fields(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        cms = CMS()
        cms.download(es_session, es_index, required_fields=["id"])

        _, actual = mock_scan.call_args
        expected = {'query': mock_dsl, 'index': 'test_index', '_source': ["id"], 'size': 100}
        self.assertEqual(actual, expected)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan_with_batch_size(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        batch_size = 10000
        cms = CMS()
        cms.download(es_session, es_index, batch_size=batch_size)

        _, actual = mock_scan.call_args
        expected = {'query': mock_dsl, 'index': 'test_index', '_source': None, 'size': batch_size}
        self.assertEqual(actual, expected)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan_with_size(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        batch_size = 1000
        expected_item = 5
        mock_scan.return_value = [{"_source": {"id": "1"}}] * batch_size
        cms = CMS()
        actual = cms.download(es_session, es_index, size=expected_item, batch_size=batch_size)
        self.assertEqual(len(actual.data), expected_item)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan_with_size_is_none(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        batch_size = 1000
        expected_item = 1000
        mock_scan.return_value = [{"_source": {"id": "1"}}] * batch_size
        cms = CMS()
        actual = cms.download(es_session, es_index, size=None, batch_size=batch_size)
        self.assertEqual(len(actual.data), expected_item)

    @patch("datax.services.cms.scan")
    @patch("datax.services.cms.CMS._add_must_conditions")
    def test_cms_call_es_scan_with_size_is_zero(self, mock_add_must_conditions, mock_scan):
        mock_dsl = mock_add_must_conditions.return_value
        es_session = MagicMock()
        es_index = "test_index"
        batch_size = 1000
        expected_item = 1000
        mock_scan.return_value = [{"_source": {"id": "1"}}] * batch_size
        cms = CMS()
        actual = cms.download(es_session, es_index, size=0, batch_size=batch_size)
        self.assertEqual(len(actual.data), expected_item)


if __name__ == "__main__":
    unittest.main()
