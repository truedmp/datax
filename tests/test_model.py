import unittest
from unittest import TestCase
from unittest.mock import patch, MagicMock

import pandas as pd

from datax.model import Datax
from datax.settings import EXTENSION_JSON, EXTENSION_CSV


class TestModelDatax(TestCase):
    def test_init_model_should_have_raw_data(self):
        data = [{"item": "a", "item": "b"}]
        actual = Datax([data])
        self.assertEqual(actual.data, data)

    def test_init_model_should_have_df(self):
        data = [{"item": "a", "item": "b"}]
        actual = Datax([data]).df
        self.assertEqual(type(actual), type(pd.DataFrame(data)))

    @patch("datax.model.model.pd")
    def test_transform_dataframe_when_data_type_is_csv(self, mock_pandas):
        data = MagicMock()
        Datax([data], data_type=EXTENSION_CSV)._transform_dataframe()
        mock_pandas.read_csv.assert_called_once_with(data, sep=",", error_bad_lines=False)

    @patch("datax.model.model.json_normalize")
    def test_transform_dataframe_when_data_type_is_json(self, mock_json_normalize):
        data = MagicMock(specs=pd.DataFrame)
        mock_json_normalize.return_value = pd.DataFrame()
        Datax([data], data_type=EXTENSION_JSON)._transform_dataframe()
        mock_json_normalize.assert_called_once_with(data)


if __name__ == "__main__":
    unittest.main()
