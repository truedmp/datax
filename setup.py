import os
from typing import List

try:
    # pip >=20
    from pip._internal.network.session import PipSession
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        # 10.0.0 <= pip <= 19.3.1
        from pip._internal.download import PipSession
        from pip._internal.req import parse_requirements
    except ImportError:
        # pip <= 9.0.3
        from pip.download import PipSession
        from pip.req import parse_requirements

from setuptools import setup, find_packages


def read_requirements() -> List[str]:
    """
    parses requirements from requirements.txt
    :return:
    """
    requirements_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')
    install_requirements = parse_requirements(requirements_path, session=PipSession())
    try:
        requirements = [str(ir.req) for ir in install_requirements]
    except:
        requirements = [str(ir.requirement) for ir in install_requirements]
    return requirements


setup(name='DataX',
      version='1.3.2',
      description='DataX is a wrapper of data services',
      author='Phasathorn Suwansri',
      author_email='phasathorn.suw@truedigital.com',
      url='https://bitbucket.org/truedmp/datax.git',
      packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
      install_requires=read_requirements()
      )
