# DataX
DataX is a wrapper of data services.

# How to use

See [Wiki: DataX - Data Wrapper Tools](https://truedmp.atlassian.net/wiki/spaces/AN/pages/688490387/DataX+-+Data+Wrapper+Tools)


# Current Version

1.3.1

# Release detail

1.3.1

- Fix `setup.py` for `pip 20.x.x`

1.3.0

- Support multiple files when downloading from GCS service


1.2.5

- Support upload dataframe with or without `header` and `index` as followed

```
gcs.upload(bucket, file, dest="/path/to/file.csv", header=True, index=True)
```

1.2.4

- Fix bug when calling .data

1.2.3

- Support unlimit size when get data from CMS

1.2.2

- Fix bug when reading csv data from GCS

1.2.1

- Support file extension specification for GCS service

1.2.0

- Support Google Cloud Storage service
	- Upload
		- from file path ("/path/to/file")
		- from dataframe object
	- Download
		- csv file
		- json file
		- other file
		- save file 
- Add helpers for google cloud storage client

1.1.0

 - Using `scan` for cms instead of `search`

1.0.0

 - Initial project
 - Support `CMS` service
 - `DataX` object

---

## Support services
- CMS (Content management system)
	- `CMS().download(ES_SESSION, ES_INDEX, REQUIRED_FIELDS, SIZE, **CONDITIONS)`


## DataX object interface
- `data` - Raw data
- `df` - Pandas DataFrame


## Example
```python
# Get Elasticsearch session
es_session = Elasticsearch(hosts=[{"host": ES_HOST, "port": ES_PORT}])

# Initial data
cms = CMS()

# Download data
datax = cms.download(
	es_session=es_session, 
	es_index="contents-movie-read",
	required_fields=["id", "title"],
	size=10,
	batch_size=1000,
	movie_type="series",
	ep_master="Y"
)


# DataX Object
print(datax)

# Raw data
print(datax.data)

# Pandas Dataframe
print(datax.df)

# Pandas DataFrame save to csv
datax.df.to_csv("result.csv")
```