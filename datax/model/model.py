from io import StringIO

import pandas as pd
from pandas.io.json import json_normalize

from ..settings import EXTENSION_JSON, EXTENSION_CSV


class Datax(object):
    """Datax Object
    Wrapper output data from datax service
    to make it easier to access.
    """
    def __init__(self, data, data_type=None):
        self.data_type = data_type
        self._df = None
        self._data = data

    def _transform_dataframe_from_csv(self):
        dfs = pd.DataFrame()
        for data in self._data:
            df = pd.read_csv(data, sep=',', error_bad_lines=False)
            dfs = pd.concat([dfs, df])
        return dfs

    def _transform_dataframe_from_json(self):
        dfs = pd.DataFrame()
        for data in self._data:
            df = json_normalize(data)
            print(df)
            dfs = pd.concat([dfs, df])
        return dfs

    def _transform_dataframe_from_other_type(self):
        dfs = pd.DataFrame()
        for data in self._data:
            df = pd.DataFrame(data)
            dfs = pd.concat([dfs, df])
        return dfs

    def _transform_dataframe(self):
        if self.data_type == EXTENSION_CSV:
            return self._transform_dataframe_from_csv()

        if self.data_type == EXTENSION_JSON:
            return self._transform_dataframe_from_json()

        return self._transform_dataframe_from_other_type()

    @property
    def data(self):
        all_data = None
        for data in self._data:
            if isinstance(data, StringIO):
                if not all_data:
                    all_data = ""
                all_data += data.read()
            else:
                if not all_data:
                    all_data = []
                all_data.extend(data)

        return all_data
    
    @property
    def df(self):
        """Return Pandas DataFrame
        """
        if self._df is None:
            self._df = self._transform_dataframe()
        return self._df

    def items(self):
        """Return items iterator
        """
        for data in self.data:
            for item in data:
                yield item
