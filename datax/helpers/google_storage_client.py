import json
from gcloud import storage
from oauth2client.service_account import ServiceAccountCredentials


def get_storage_client(project, credentials_path):
    # Load secrets from file
    google_api_secret = json.load(open(credentials_path))

    # Make credentials object
    credentials_dict = {
        'type': 'service_account',
        'client_id': google_api_secret['client_id'],
        'client_email': google_api_secret['client_email'],
        'private_key': google_api_secret['private_key'],
        'private_key_id': google_api_secret['private_key_id']
    }

    credentials = ServiceAccountCredentials.from_json_keyfile_dict(
        credentials_dict
    )
    storage_client = storage.Client(credentials=credentials, project=project)
    return storage_client