from elasticsearch.helpers import scan
from loguru import logger as _logger

from .service import Service
from ..model import Datax


class CMS(Service):
    def __init__(self):
        """CMS service
        Use to download or get data from content management system.
        Currently, we connect to the elasticsearch directly.

        Example:
            > cms = CMS()
            > datax = cms.download(es_session, es_index)
            > datax.data
            Raw data in DataX object
        """
        super().__init__()
        self.dsl = None

    def _initial_dsl(self):
        """Initial dsl for elasticsearch
        """
        return {"query": {"bool": {"filter": {"bool": {"must": []}}}}}

    def _add_must_conditions(self, dsl, **conditions):
        """Loop to add must conditions
        """
        must_conditions = []
        if conditions:
            for key, value in conditions.items():
                must_conditions.append({"match": {key: value}})

        dsl['query']['bool']['filter']['bool']['must'] = must_conditions
        return dsl

    def download(self, es_session, es_index, required_fields=None, size=None, batch_size=100, **conditions):
        """Download data from elasticsearch from specify conditions
        
        Example:
            > datax = cms.download(es_session, es_index, required_fields=["id", "title"], movie_type="move", ep_master="Y")
            > datax
            // DataX Object
    
        :param:
            es_session - Elasticsearch session
            es_index - Elasticseach index name (eg. 'contents-movie-read')
            required_fields - List of required fields (eg. ['title', 'id'])
            size - Size of items you want (default: 10)
            batch_size - Size of bulk query from elasticsearch (default: 100)
            **conditions - kwargs as a conditions (movie_type="movie", ep_master="Y")
        :return:
            DataX Object
        """
        self.dsl = self._initial_dsl()
        self.dsl = self._add_must_conditions(self.dsl, **conditions)

        _logger.debug(self.dsl)

        try:
            self.data = []
            for item in scan(es_session, query=self.dsl, index=es_index, _source=required_fields, size=batch_size):
                self.data.append(item.get("_source", {}))
                if size and len(self.data) >= size:
                    break
        except Exception as error:
            _logger.error(f"{type(error)}: {error}")

        return Datax([self.data])
