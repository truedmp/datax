class Service:
    def __init__(self):
        self.data = None
        self.df = None

    def download(self, **kwargs):
        """Download data from some service
        """
        raise NotImplementedError
