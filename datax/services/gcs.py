import pathlib
from io import StringIO
from typing import Union

import pandas as pd
from gcloud.storage import Bucket
from loguru import logger as _logger

from .service import Service
from ..model.model import Datax


class GCS(Service):
    def __init__(self):
        self.data_type = None
        super().__init__()

    @staticmethod
    def _upload_file(bucket, file, destination, content_type=None):
        blob = bucket.blob(destination)
        return blob.upload_from_filename(file, content_type=content_type)

    @staticmethod
    def _upload_dataframe(bucket, file, dest, index=False, header=False):
        content = StringIO()
        file.to_csv(content, index=index, header=header)
        blob = bucket.blob(dest)
        return blob.upload_from_string(content.getvalue(), content_type='text/csv')

    def upload(self, bucket: Bucket, file: Union[str, pd.DataFrame], dest: str, index: bool = False, header: bool = False, content_type: str = None):
        """
        Upload file, or pandas data frame into google cloud storage.
        :param bucket: Google Cloud Storage Bucket.
        :param file: File's path, pandas data frame.
        :param dest: Destination path on google cloud storage.
        :param index: If a file is a dataframe this is an option to save output with index.
        :param header: If a file is a dataframe this is an option to save output with headers.
        :param content_type: Optional type of content being uploaded.
        :return: True if success.
        """
        _logger.debug(f"Uploading file to {dest}")

        try:
            if isinstance(file, str):
                self._upload_file(bucket, file, dest, content_type)
            elif isinstance(file, pd.DataFrame):
                self._upload_dataframe(bucket, file, dest, index, header)
        except Exception as error:
            _logger.error(error)
            raise TypeError(error)

        _logger.success(f"Upload successful.")
        return True

    @staticmethod
    def _download_file_as_string(blob):
        try:
            content = str(blob.download_as_string(), 'utf-8')
        except Exception:
            content = str(blob.download_as_string())
        return StringIO(content)

    @staticmethod
    def _get_file_extension(blobs):
        files_extension = set([pathlib.Path(blob.name).suffix for blob in blobs])
        if len(files_extension) > 1:
            raise TypeError(f"All files should have the same file extensions!. We found {files_extension}")
        return list(files_extension)[0]

    @staticmethod
    def _download_to_destination(bucket, blob, destination):
        filename = pathlib.Path(blob.name).name
        destination = pathlib.Path(destination)
        destination.mkdir(parents=True, exist_ok=True)
        output_path = destination.joinpath(filename)

        _logger.debug(f"Saving file to {output_path}")
        try:
            blob_data = bucket.blob(blob.name)
            blob_data.download_to_filename(output_path)
            _logger.success(f"Saving file to {output_path} [DONE]")
        except Exception as error:
            _logger.error(error)

    @staticmethod
    def _get_blobs(bucket, path):
        blobs = bucket.list_blobs(prefix=path)
        blobs = list(blobs)
        _logger.debug(f"We found {len(blobs)} files.")
        return blobs

    def download(self, bucket: Bucket, path: str, file_extension: str = None, output_path: str = None) -> Datax:
        """
        Download or retrieve files from google cloud storage
        :param bucket: Google Cloud Storage Bucket.
        :param path: File's path on google cloud storage.
        :param file_extension: File's extension (".csv", ".json")
        :param output_path: If specified, it will download and save to this path
        :return: DataX object
        """
        self.data = []

        blobs = self._get_blobs(bucket, path)
        self.data_type = self._get_file_extension(blobs)

        for blob in blobs:
            _logger.debug(f"Downloading: {blob.name}")
            blob_data = bucket.blob(blob.name)
            content = self._download_file_as_string(blob_data)
            self.data.append(content)

            if output_path:
                self._download_to_destination(bucket, blob, output_path)

        return Datax(self.data, data_type=self.data_type)
